// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDQu4Gv15w01Bo_9VuWnbKf2Drx3af2t_8",
    authDomain: "fir-demo-abb9c.firebaseapp.com",
    databaseURL: "https://fir-demo-abb9c.firebaseio.com",
    projectId: "fir-demo-abb9c",
    storageBucket: "fir-demo-abb9c.appspot.com",
    messagingSenderId: "72561704105",
    appId: "1:72561704105:web:a0bef0c06526050ed67db8",
    measurementId: "G-127E1L84P9"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
