import { Component ,OnInit} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title="firebase-demo"
  allcourses:any[]
  courses:Observable<any[]>;
  constructor( db:AngularFireDatabase){
        this.courses = db.list('/courses').valueChanges();
        this.courses.subscribe(courses=>{
        this.allcourses = courses;
        console.log(this.allcourses);
    })
    console.log(this.courses)
  }
}