import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn:'root'
})
export class WeatherService {
  
  constructor(private _http:HttpClient) {}
    dailyForecast(){
      return this._http.get("https://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=439d4b804bc8187953eb36d2a8c26a02");
    }
   
}
