import { Component } from '@angular/core';
import {UsersService} from './users.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-basics';
  firstname = 'yamini';
  lastname = 'chakka';
  email = 'yaminikanakarao@gmail.com'
  age = 21;
  city = 'Hyderabad';
  hobbies = ['music','sketching']
  address = {
      hno : '1-58/1',
      street : 'Tekulapalle',
      district : 'Bhadradhi Kothagudem'
  }
  firstnumber = 0;
  secondnumber = 0;
  sum = 0;
  getAdd(num1,num2){
    this.sum = parseFloat(num1) + parseFloat(num2);
  }
  color = '';
  changeColor(color){
    this.color = color;
  }
  text = '';
  handletext(text){
    this.text = text;
  }
  success = 'success';
  msg='';
  names = ['yamini','shravya','priyanka','navya']
  users = [
    {
      name:'yamini',
      email:'yaminikanakarao@gmail.com',
      number:'9705163493'
    },
    {
      name:'shravya',
      email:'shravyachakinam19@gmail.com',
      number:'9676800881'
    },
    {
      name:'priyanka',
      email:'priyankacheruku16@gmail.com',
      number:'9234567891'
    }
  ]
  allUsers = null
  constructor(private user:UsersService){
      this.allUsers = user.getAllUser();
  }
}